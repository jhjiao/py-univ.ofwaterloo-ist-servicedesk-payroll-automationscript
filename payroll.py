import openpyxl as pyxl
payroll = pyxl.load_workbook ('D:/IST/Payroll/schedule.xlsx')
outputs = pyxl.load_workbook ('D:/IST/Payroll/middle.xlsx')
output_sheet = outputs.active
coops = ['Moayyad Omer Al-Omari','Zhicheng Zeng','Eric Kwon','Farhia Hussein','Amin Masum','Laura Suter','Nafeem Rahman','Jia Hua Jiao','Jamie Kiyoko Shigeishi']
scc_name = ['Asad, Omer','Bahri, Rishabh','Bergeron, Isaac','Bouchard, Bethany','Goodwin, Emily','Hassan, Sayeed','Iyer, Monica','Kumar, Aishwarya','Le, Vi','Lepage, Renee','Lu, Joyce','Morin, Samantha','Ruza, Tia','Sharma, Nandani', 'Stoicoiu, Elisa','Tran, Vincent']

scc_info = {'Omer Asad': [292739, 17.5, 'Asad, Omer'], 
            'Rishab Bahri': [249292, 18.5, 'Bahri, Rishab'], 
            'Isaac Bergeron': [285904, 17.5, 'Bergeron, Isaac'], 
            'Bethany Bouchard': [249714, 19, 'Bouchard, Bethany'],
            'Emily Goodwin': [262792, 19.5, 'Goodwin, Emily'],
            'Sayeed Hassan': [292738, 17.5, 'Hassan, Sayeed'],
            'Monica Iyer': [261789, 18.0, 'Iyer, Monica'],
            'Aishwarya Kumar': [258960, 19.5, 'Kumar, Aishwarya'],
            'Vi Le': [269888, 19.0, 'Le, Vi'],
            'Renee Lepage': [292533,17.0, 'Lepage, Renee'],
            'Joyce Lu': [260161, 17.0, 'Lu, Joyce'],
            'Samantha Morin': [263304, 19.0, 'Morin, Samantha'],
            'Tia Ruza': [286945, 19.5, 'Ruza, Tia'],
            'Nandani Sharma': [289272, 17.5, 'Sharma, Nandani'],
            'Vincent Tran': [253390, 18.5, 'Tran, Vincent'],
            'Elisa Stoicoiu': [292344, 17.0, 'Stoicoiu, Elisa']}
scc_id = [292739,249292,285904,249714,262792,292738,261789,258960,269888,292533,260161,263304,286945,289272,292344,253390]
payroll_sheet = payroll.active
rows = payroll_sheet.max_row

hours_worked = {'8:30 AM - 11:30 AM': 3, '12:30 PM - 4:30 PM': 4, '11 AM - 1 PM': 2, '4:30 PM - 8:30 PM': 4}

dates = []
field_names = payroll_sheet ['1']
for date in field_names:
    dates.append (date.value)

print (dates)
print (type(dates))
for index, row in enumerate(payroll_sheet.iter_rows (min_row = 4, max_row = rows, min_col = 1, max_col = 17)):
    list1 = []
    for cell in row:
        list1.append (cell.value)
        
    if list1[0] in coops:
        continue
    if list1[1] in ('DC', 'DP'):
        cost_ctr = 'IST'
    else:
        cost_ctr = 'Housing'
    
    for i in range (2, 17):
        if list1[i] != '':
            try:
                hours = hours_worked [str(list1[i])]
                date_worked = dates [i]
                new_record = tuple([scc_info[list1[0]][0], scc_info[list1[0]][2], 'REG', date_worked + ', 2020', hours, '', scc_info[list1[0]][1], '', hours * scc_info[list1[0]][1],cost_ctr])
                output_sheet.append (new_record)
                print (new_record)
            except KeyError:
                continue

outputs.save ('D:/IST/Payroll/middle.xlsx')
        
    
    

